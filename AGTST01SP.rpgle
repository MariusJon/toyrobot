000100 180819      **FREE                                                                                         
000200 180819        ctl-opt Nomain Option(*srcstmt:*nodebugio);                                                  
000300 180819        /include AGTST01P                                                                            
000400 180819                                                                                                     
000500 180819        //-------------------------------------------------------------------                        
000600 180819        // Toy Robot Simulation - Services                                                           
000700 180819        //-------------------------------------------------------------------                        
000800 180819        dcl-proc processCommand export;                                                              
000900 180819          dcl-pi processCommand char(50);                                                            
001000 180819            i_Robot LikeDs(t_Robot);                                                                 
001100 180819            i_Command char(50);                                                                      
001200 180819          end-pi;                                                                                    
001300 180819                                                                                                     
001400 180819          //Local Vars:                                                                              
001500 180819          dcl-ds l_cmd LikeDs(t_CommandDs);                                                          
001600 180819          dcl-s  l_msg char(50);                                                                     
001700 180819                                                                                                     
001800 180819          //Explicitly clear output vars:                                                            
001900 180819          l_msg = *Blanks;                                                                           
002000 180819                                                                                                     
002100 180819          //Basics first, do we have a command string?                                               
002200 180819          If i_Command = *Blank;                                                                     
002300 180819             // Drop out;                                                                            
002400 180819             return l_msg;                                                                           
002500 180819          EndIf;                                                                                     
002600 180819                                                                                                     
002700 180819          //Extricate the command part from the string;                                              
002800 180819          l_cmd = splitCommand(i_Command);                                                           
002900 180819                                                                                                     
003000 180819          //Call the appropriate routine per command:                                                
003100 180819          Select;                                                                                    
003200 180819             when l_cmd.command = 'PLACE';                                                           
003300 180819               processPlace(i_robot:l_cmd);                                                          
003400 180819             when l_cmd.command = 'LEFT';                                                            
003500 180819               processLeft(i_robot);                                                                 
003600 180819             when l_cmd.command = 'RIGHT';                                                           
003700 180819               processRight(i_robot);                                                                
003800 180819             when l_cmd.command = 'MOVE';                                                            
003900 180819               processMove(i_robot);                                                                 
004000 180819             when l_cmd.command = 'REPORT';                                                          
004100 180819               l_msg = processReport(i_robot);                                                       
004200 180819          EndSl;                                                                                     
004300 180819                                                                                                     
004400 180819          return l_msg;                                                                              
004500 180819        end-proc;                                                                                    
004600 180819                                                                                                     
004700 180819        dcl-proc splitCommand;                                                                       
004800 180819          dcl-pi splitCommand LikeDs(t_CommandDs);                                                   
004900 180819            i_Command char(50);                                                                      
005000 180819          end-pi;                                                                                    
005100 180819                                                                                                     
005200 180819          // Local Vars                                                                              
005300 180819          dcl-ds l_cmd LikeDs(t_CommandDs) inz;                                                      
005400 180819          dcl-s  l_pos int(10:0);                                                                    
005500 180819          dcl-s  x     int(10:0);                                                                    
005600 180819                                                                                                     
005700 180819          //Explicitly clear output var:                                                             
005800 180819          clear l_cmd;                                                                               
005900 180819                                                                                                     
006000 180819          //Remove any possible blanks on left:                                                      
006100 180819          i_Command = %TrimL(i_Command);                                                             
006200 180819                                                                                                     
006300 180819          //Convert the cmd to upper case:                                                           
006400 180819          Exec Sql set :i_Command = UPPER(:i_Command);                                               
006500 180819                                                                                                     
006600 180819          //Check if its a place command:                                                            
006700 180819          If %Scan('PLACE':i_Command) > 0;                                                           
006800 180819            //Get the first space to separate the values from the keyword                            
006900 180819            l_pos = %Scan(' ':i_Command);                                                            
007000 180819            l_Cmd.command = %SubSt(i_Command:1:l_Pos - 1);                                           
007100 180819                                                                                                     
007200 180819            // If first word is not PLACE, leave                                                     
007300 180819            If l_Cmd.command <> 'PLACE';                                                             
007400 180819              clear l_Cmd;                                                                           
007500 180819              Return l_Cmd;                                                                          
007600 180819            EndIf;                                                                                   
007700 180819                                                                                                     
007800 180819            i_Command = %SubSt(i_Command:l_Pos + 1);                                                 
007900 180819            x = 0;                                                                                   
008000 180819            l_Pos = %Scan(',':i_Command);                                                            
008100 180819            DoW l_Pos > 0 and x <= %Size(l_cmd.values);                                              
008200 180819              x += 1;                                                                                
008300 180819              Monitor; //Might be some nasty non numerics in there:                                  
008400 180819                 l_cmd.values(x) = %int(%SubSt(i_command:1:l_Pos - 1));                              
008500 180819              on-error;                                                                              
008600 180819                 clear l_Cmd;                                                                        
008700 180819                 Return l_Cmd;                                                                       
008800 180819              EndMon;                                                                                
008900 180819              i_Command = %SubSt(i_Command:l_Pos + 1);                                               
009000 180819              l_Pos = %Scan(',':i_Command);                                                          
009100 180819            EndDo;                                                                                   
009200 180819            // Do the last parm                                                                      
009300 180819            If %Len(%Trim(i_Command)) > 0;                                                           
009400 180819              If Not(x < %Size(l_cmd.values));                                                       
009500 180819                 //No more room, command not valid                                                   
009600 180819                 clear l_Cmd;                                                                        
009700 180819                 Return l_Cmd;                                                                       
009800 180819              EndIf;                                                                                 
009900 180819                                                                                                     
010000 180819              x += 1;                                                                                
010100 180819              // Is the direction, convert it to a value:                                            
010200 180819              Select;                                                                                
010300 180819                When i_Command = 'NORTH';                                                            
010400 180819                  l_cmd.values(x) = North;                                                           
010500 180819                When i_Command = 'EAST';                                                             
010600 180819                  l_cmd.values(x) = East;                                                            
010700 180819                When i_Command = 'South';                                                            
010800 180819                  l_cmd.values(x) = South;                                                           
010900 180819                When i_Command = 'WEST';                                                             
011000 180819                  l_cmd.values(x) = West;                                                            
011100 180819                Other;                                                                               
011200 180819                   //Direction not valid                                                             
011300 180819                   clear l_Cmd;                                                                      
011400 180819                   Return l_Cmd;                                                                     
011500 180819              EndSl;                                                                                 
011600 180819            EndIf;                                                                                   
011700 180819          Else;                                                                                      
011800 180819            //Only one word, Check if it's one of the valid ones:                                    
011900 180819            Select;                                                                                  
012000 180819              When i_Command = 'MOVE';                                                               
012100 180819              When i_Command = 'LEFT';                                                               
012200 180819              When i_Command = 'RIGHT';                                                              
012300 180819              When i_Command = 'REPORT';                                                             
012400 180819              Other;                                                                                 
012500 180819                 //Command is not valid                                                              
012600 180819                 clear l_Cmd;                                                                        
012700 180819                 Return l_Cmd;                                                                       
012800 180819            EndSl;                                                                                   
012900 180819            l_cmd.command = i_command;                                                               
013000 180819          EndIf;                                                                                     
013100 180819                                                                                                     
013200 180819          Return l_cmd;                                                                              
013300 180819        end-proc;                                                                                    
013400 180819                                                                                                     
013500 180819        dcl-proc processPlace;                                                                       
013600 180819          dcl-pi processPlace;                                                                       
013700 180819            i_Robot LikeDs(t_Robot);                                                                 
013800 180819            l_cmd LikeDs(t_CommandDs);                                                               
013900 180819          end-pi;                                                                                    
014000 180819                                                                                                     
014100 180819          //Check whether input is OK:                                                               
014200 180819          if Not(l_cmd.values(1) < pf_max_x                                                          
014300 180819             And l_cmd.values(2) < pf_max_y);                                                        
014400 180819             //Coordinates not cool, ignore place cmd                                                
014500 180819             Return;                                                                                 
014600 180819          EndIf;                                                                                     
014700 180819                                                                                                     
014800 180819          //Set robot:                                                                               
014900 180819          i_robot.x = l_cmd.values(1);                                                               
015000 180819          i_robot.y = l_cmd.values(2);                                                               
015100 180819          i_robot.direction = l_cmd.values(3);                                                       
015200 180819                                                                                                     
015300 180819          Return;                                                                                    
015400 180819        end-proc;                                                                                    
015500 180819                                                                                                     
015600 180819        dcl-proc processLeft;                                                                        
015700 180819          dcl-pi processLeft;                                                                        
015800 180819            i_Robot LikeDs(t_Robot);                                                                 
015900 180819          end-pi;                                                                                    
016000 180819          i_robot.direction -= 1;                                                                    
016100 180819          If i_robot.direction < 0;                                                                  
016200 180819             i_robot.direction = west;                                                               
016300 180819          EndIf;                                                                                     
016400 180819        end-proc;                                                                                    
016500 180819                                                                                                     
016600 180819        dcl-proc processRight;                                                                       
016700 180819          dcl-pi processRight;                                                                       
016800 180819            i_Robot LikeDs(t_Robot);                                                                 
016900 180819          end-pi;                                                                                    
017000 180819          i_robot.direction += 1;                                                                    
017100 180819          If i_robot.direction > west;                                                               
017200 180819             i_robot.direction = north;                                                              
017300 180819          EndIf;                                                                                     
017400 180819        end-proc;                                                                                    
017500 180819                                                                                                     
017600 180819        dcl-proc processMove;                                                                        
017700 180819          dcl-pi processMove;                                                                        
017800 180819            i_Robot LikeDs(t_Robot);                                                                 
017900 180819          end-pi;                                                                                    
018000 180819                                                                                                     
018100 180819          Select;                                                                                    
018200 180819             When i_robot.direction = north;                                                         
018300 180819                If i_robot.y + 1 <= pf_max_y;                                                        
018400 180819                   i_robot.y += 1;                                                                   
018500 180819                EndIf;                                                                               
018600 180819             When i_robot.direction = south;                                                         
018700 180819                If i_robot.y - 1 > 0;                                                                
018800 180819                   i_robot.y -= 1;                                                                   
018900 180819                EndIf;                                                                               
019000 180819             When i_robot.direction = east;                                                          
019100 180819                If i_robot.x + 1 <= pf_max_x;                                                        
019200 180819                   i_robot.x += 1;                                                                   
019300 180819                EndIf;                                                                               
019400 180819             When i_robot.direction = west;                                                          
019500 180819                If i_robot.x - 1 > 0;                                                                
019600 180819                   i_robot.x -= 1;                                                                   
019700 180819                EndIf;                                                                               
019800 180819          EndSl;                                                                                     
019900 180819                                                                                                     
020000 180819          Return;                                                                                    
020100 180819        end-proc;                                                                                    
020200 180819                                                                                                     
020300 180819        dcl-proc processReport;                                                                      
020400 180819          dcl-pi processReport char(50);                                                             
020500 180819            i_Robot LikeDs(t_Robot);                                                                 
020600 180819          end-pi;                                                                                    
020700 180819                                                                                                     
020800 180819          //Local Vars:                                                                              
020900 180819          dcl-s l_msg char(50);                                                                      
021000 180819                                                                                                     
021100 180819          l_msg = %Char(i_Robot.x) + ',' + %Char(i_Robot.y) + ',';                                   
021200 180819          Select;                                                                                    
021300 180819            When i_robot.direction = north;                                                          
021400 180819              l_msg = %Trim(l_msg) + 'NORTH';                                                        
021500 180819            When i_robot.direction = south;                                                          
021600 180819              l_msg = %Trim(l_msg) + 'SOUTH';                                                        
021700 180819            When i_robot.direction = east;                                                           
021800 180819              l_msg = %Trim(l_msg) + 'EAST';                                                         
021900 180819            When i_robot.direction = west;                                                           
022000 180819              l_msg = %Trim(l_msg) + 'WEST';                                                         
022100 180819          EndSl;                                                                                     
022200 180819                                                                                                     
022300 180819          Return l_msg;                                                                              
022400 180819        end-proc;                                                                                    
