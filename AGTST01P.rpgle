000100 180819        /if defined(AGTST01P)                                                                        
000200 180819        /eof                                                                                         
000300 180819        /endif                                                                                       
000400 180819        /define AGTST01P                                                                             
000500 180819        //-------------------------------------------------------------------                        
000600 180819        // Toy Robot Simulation - Constants, Templates                                               
000700 180819        //-------------------------------------------------------------------                        
000800 180819                                                                                                     
000900 180819        // Declare the directions as constants:                                                      
001000 180819        dcl-c North const(0);                                                                        
001100 180819        dcl-c East  const(1);                                                                        
001200 180819        dcl-c South const(2);                                                                        
001300 180819        dcl-c West  const(3);                                                                        
001400 180819        // Declare the playfield size constants:                                                     
001500 180819        dcl-c pf_max_x const(4);                                                                     
001600 180819        dcl-c pf_max_y const(4);                                                                     
001700 180819                                                                                                     
001800 180819        // Declare a template DS for the Robot:                                                      
001900 180819        dcl-ds t_Robot qualified template;                                                           
002000 180819                  x         int(10:0);                                                               
002100 180819                  y         int(10:0);                                                               
002200 180819                  direction int(10:0);                                                               
002300 180819        End-Ds;                                                                                      
002400 180819                                                                                                     
002500 180819        // Declare a template DS for the Command and its values;                                     
002600 180819        dcl-ds t_CommandDs qualified template;                                                       
002700 180819                  command char(10) inz;                                                              
002800 180819                  values  int(10:0) dim(5) inz;                                                      
002900 180819        End-Ds;                                                                                      
003000 180819                                                                                                     
003100 180819        //-------------------------------------------------------------------                        
003200 180819        // Toy Robot Simulation - Prototypes                                                         
003300 180819        //-------------------------------------------------------------------                        
003400 180819        dcl-pr processCommand char(50);                                                              
003500 180819          i_Robot LikeDs(t_Robot);                                                                   
003600 180819          i_Command char(50);                                                                        
003700 180819        end-pr;                                                                                      
