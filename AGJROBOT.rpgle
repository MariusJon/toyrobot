000100 180815      **free                                                                                         
000200 180819        ctl-opt bnddir('AGTST01SP');                                                                 
000300 180819        ctl-opt ActGrp('AGTST');                                                                     
000400 180819        ctl-opt main(Main);                                                                          
000500 180819        /include AGTST01P                                                                            
000600 180819          dcl-pr Main extpgm('AGJROBOT') ;                                                           
000700 180819            *n char(30);                                                                             
000800 180819            *n char(30);                                                                             
000900 180819          end-pr ;                                                                                   
001000 180819                                                                                                     
001100 180819          dcl-proc Main ;                                                                            
001200 180819             dcl-pi *n ;                                                                             
001300 180819               infile char(30);                                                                      
001400 180819               outfile char(30);                                                                     
001500 180819             end-pi ;                                                                                
001600 180819            // Local Vars:                                                                           
001700 180819            Dcl-Ds l_Robot LikeDs(t_Robot);                                                          
001800 180819            Dcl-S  l_Cmd Char(50);                                                                   
001900 180819            Dcl-S  l_Rpt Char(50);                                                                   
002000 180819            Dcl-S  l_stmt Char(150);                                                                 
002100 180819            Dcl-S  l_insrt Char(150);                                                                
002200 180819                                                                                                     
002300 180819            Exec SQL Set Option COMMIT = *NONE;                                                      
002400 180819                                                                                                     
002500 180819            l_stmt = 'select txt from ' + infile;                                                    
002600 180819            Exec SQL PREPARE S1 FROM :l_stmt;                                                        
002700 180819            l_insrt = 'insert into ' + outfile + 'VALUES(?)';                                        
002800 180819            Exec SQL PREPARE S2 FROM :l_insrt;                                                       
002900 180819                                                                                                     
003000 180819            Exec SQL DECLARE C1 CURSOR FOR S1;                                                       
003100 180819                                                                                                     
003200 180819            Exec SQL Open C1;                                                                        
003300 180819                                                                                                     
003400 180819            Exec SQL Fetch Next from C1 into :l_cmd;                                                 
003500 180819            Dow SQLSTATE = '00000';                                                                  
003600 180819               l_rpt = processCommand(l_Robot:l_Cmd);                                                
003700 180819                                                                                                     
003800 180819               If l_rpt <> *blanks;                                                                  
003900 180819                  Exec SQL Execute S2 using :l_rpt;                                                  
004000 180819               EndIf;                                                                                
004100 180819               Exec SQL Fetch Next from C1 into :l_cmd;                                              
004200 180819            EndDo;                                                                                   
004300 180819            *inLr = *on;                                                                             
004400 180819            Return;                                                                                  
004500 180819          End-Proc;                                                                                  
